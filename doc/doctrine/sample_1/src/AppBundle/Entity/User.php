<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpKernel\Bundle\Bundle;


/** 
  * @ORM\Entity
  * @ORM\Table(name="s_users")
  */
class User
{
	/** 
	  * @ORM\Column(type="integer",name="userId",nullable=false)
	  * @ORM\Id
	  * @ORM\GeneratedValue(strategy="AUTO")
	  */
	private $userId;

	/** 
	  * @ORM\Column(type="string",name="userName",length=25,unique=true,nullable=false)
	  */
	private $name;

	/** 
	  * @ORM\Column(type="string",name="nickName",length=50,nullable=true)
	  */
	private $nickName;
    
	/** 
	  * @ORM\Column(type="string",name="userPwd",length=25,nullable=false)
	  */
	private $pwd;
	
	/** 
      * @ORM\OneToOne(targetEntity="Student")
      * @ORM\JoinColumn(name="studentID", referencedColumnName="studentId")	  
      */
	private $student;
    
	/** 
	  * @return mixed
	  */
	public function getUserId()
	{
		return $this->userId;
	}

	/** 
	  * @return mixed
	  */
	public function getName()
	{
		return $this->name;
	}

	/** 
	  * @return mixed
	  */
	public function getNickName()
	{
		return $this->nickName;
	}
    
	/** 
	  * @return mixed
	  */
	public function getPwd()
	{
		return $this->pwd;
	}

	/** 
	  * @return mixed
	  */
	public function getStudent()
	{
		return $this->student;
	}
	  
	/** 
	  * @param mixed $id
	  */
	public function setUserId($userId)
	{
		$this->userId = $userId;
	}
	  
	/** 
	  * @param mixed $name
	  */
	public function setName($name)
	{
		$this->name = $name;
	}

	/** 
	  * @param mixed $name
	  */
	public function setNickName($nickName)
	{
		$this->nickName = $nickName;
	}
    
	/** 
	  * @param mixed $pwd
	  */
	public function setPwd($pwd)
	{
		$this->pwd = $pwd;
	}

	/** 
	  * @param mixed $student
	  */
	public function setStudent($student)
	{
		$this->student = $student;
	}
    
}

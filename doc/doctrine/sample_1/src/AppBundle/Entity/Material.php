<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/** 
  * @ORM\Entity
  * @ORM\Table(name="s_materials",
  *   uniqueConstraints={
  *     @ORM\UniqueConstraint(name="material_idx", columns={"materialName", "session"})
  *   }
  *  )
  */
class Material
{
	/** 
	  * @ORM\Column(type="integer",name="materialId")
	  * @ORM\Id
	  * @ORM\GeneratedValue(strategy="AUTO")
      * @ORM\OneToMany(targetEntity="Mark",mappedBy="materialId")
	  */
	private $materialId;

	/** 
	  * @ORM\Column(type="string",name="materialName",nullable=false,length=50)
	  */
	private $name;

	/** 
	  * @ORM\Column(type="string",name="session",nullable=false,length=1)
	  */
	private $session;
    
	/** 
	  * @return materialId
	  */
	public function getMaterialId()
	{
		return $this->materialId;
	}

	/** 
	  * @return name
	  */
	public function getName()
	{
		return $this->name;
	}

	/** 
	  * @return session
	  */
	public function getSession()
	{
		return $this->session;
	}
    
	/** 
	  * @param set materialId
	  */
	public function setMaterialId($materialId)
	{
		$this->materialId = $materialId;
	}
	  
	/** 
	  * @param set name
	  */
	public function setName($name)
	{
		$this->name = $name;
	}

	/** 
	  * @param set session
	  */
	public function setSession($session)
	{
		$this->session = $session;
	}
    
	/** 
	  * @param to be displayed by Material lookup: 
      * display: material name with session
      * refer: materialId
	  */
    public function __toString() 
    {
        if (is_null($this->session))
        {
            return $this->name;
        }
        else
        {
            return $this->name . " - " . $this->session;
        }
    }    
    
}


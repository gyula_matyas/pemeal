<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/** 
  * @ORM\Entity
  * @ORM\Table(name="s_marks",
  *   uniqueConstraints={
  *     @ORM\UniqueConstraint(name="mark_idx", columns={"materialId", "studentId"})
  *   }
  *  )
  */
class Mark
{
	/** 
	  * @ORM\Column(type="integer",name="markId",nullable=false)
	  * @ORM\Id
	  * @ORM\GeneratedValue(strategy="AUTO")
	  */
	private $id;

	/** 
      * @ORM\ManyToOne(targetEntity="Student")
      * @ORM\JoinColumn(name="studentID", referencedColumnName="studentId",onDelete="CASCADE",nullable=false)
      */
	private $student;

	/** 
      * @ORM\ManyToOne(targetEntity="Material")
      * @ORM\JoinColumn(name="materialID", referencedColumnName="materialId",nullable=false)	  
	  */
	private $material;

  	/** 
	  * @ORM\Column(type="integer",name="mark",nullable=false)
	  */
	private $mark;

	/** 
	  * @return markId
	  */
	public function getId()
	{
		return $this->id;
	}

	/** 
	  * @return mark
	  */
	public function getMark()
	{
		return $this->mark;
	}

    /** 
	  * @return student
	  */
	public function getStudent()
	{
		return $this->student;
	}

    /** 
	  * @return material
	  */
	public function getMaterial()
	{
		return $this->material;
	}
    
	/** 
	  * @param set markId
	  */
	public function setId($id)
	{
		$this->id = $id;
	}
	  
	/** 
	  * @param set mark
	  */
	public function setMark($mark)
	{
		$this->mark = $mark;
	}

   	/** 
	  * @param set student
	  */
	public function setStudent($student)
	{
		$this->student = $student;
	}

  	/** 
	  * @param set material
	  */
	public function setMaterial($material)
	{
		$this->material = $material;
	}
}


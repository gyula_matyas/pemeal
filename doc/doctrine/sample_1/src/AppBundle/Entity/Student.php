<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/** 
  * @ORM\Entity
  * @ORM\Table(name="s_students")
  */
class Student
{
	/** 
	  * @ORM\Column(type="integer",name="studentId",nullable=false)
	  * @ORM\Id
	  * @ORM\GeneratedValue(strategy="AUTO")
      * @ORM\OneToOne(targetEntity="User",mappedBy="studentId")
      * @ORM\OneToMany(targetEntity="Mark",mappedBy="studentId",cascade={"persist","remove"})
	  */
	private $studentId;

	/** 
	  * @ORM\Column(type="string",name="studentName",length=50,unique=true,nullable=false)
	  */
	private $name;

	/** 
	  * @ORM\Column(type="string",name="fatherName",length=25,nullable=false)
	  */
	private $fatherName;
    
	/** 
	  * @return $studentId
	  */
	public function getStudentId()
	{
		return $this->studentId;
	}

	/** 
	  * @return $name
	  */
	public function getName()
	{
		return $this->name;
	}

   	/** 
	  * @return $fatherName
	  */
	public function getFatherName()
	{
		return $this->fatherName;
	}

	/** 
	  * @param mixed $studentId
	  */
	public function setStudentId($studentId)
	{
		$this->studentId = $studentId;
	}
	  
	/** 
	  * @param mixed $name
	  */
	public function setName($name)
	{
		$this->name = $name;
	}

	/** 
	  * @param mixed $fatherName
	  */
	public function setFatherName($fatherName)
	{
		$this->fatherName = $fatherName;
	}
    
	/** 
	  * @param to be displayed by Student lookup: 
      * display: student name with father name
      * refer: studentId
	  */
    public function __toString() 
    {
        return ($this->name . " (" . $this->fatherName . ")");
    }    
    
}

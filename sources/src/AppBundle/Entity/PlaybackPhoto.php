<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/** 
  * @ORM\Entity
  * @ORM\Table(name="pma_playbackphotos",
  *   uniqueConstraints={
  *     @ORM\UniqueConstraint(name="playbackphotos_idx", columns={"playbackId", "photoId"})
  *   }
  *  )
  */
class PlaybackPhoto
{
	/** 
	  * @ORM\Column(type="integer",name="itemId",nullable=false)
	  * @ORM\Id
	  */
	private $itemId;

	/** 
      * @ORM\ManyToOne(targetEntity="Playback")
      * @ORM\JoinColumn(name="playbackId", referencedColumnName="playbackId",onDelete="CASCADE",nullable=false)
	  */
	private $playback;

	/** 
      * @ORM\ManyToOne(targetEntity="Photo")
      * @ORM\JoinColumn(name="photoId", referencedColumnName="photoId",onDelete="CASCADE",nullable=false)
	  */
	private $photo;

	/** 
	  * @ORM\Column(type="smallint",name="pos",nullable=false,options={"default":0})
	  */
	private $pos;

	/** 
	  * @ORM\Column(type="integer",name="delay",nullable=false,options={"default":0})
	  */
	private $delay;

	/** 
	  * @return $itemId
	  */
	public function getItemId()
	{
		return $this->itemId;
	}

	/** 
	  * @return $playback
	  */
	public function getPlayback()
	{
		return $this->playback;
	}

	/** 
	  * @return $photo
	  */
	public function getPhoto()
	{
		return $this->photo;
	}

	/** 
	  * @return $pos
	  */
	public function getPos()
	{
		return $this->pos;
	}

	/** 
	  * @return $delay
	  */
	public function getDelay()
	{
		return $this->delay;
	}

	/** 
	  * @param $itemId
	  */
	public function setItemId($itemId)
	{
		$this->itemId = $itemId;
		return $this;
	}

	/** 
	  * @param $playback
	  */
	public function setPlayback($playback)
	{
		$this->playback = $playback;
		return $this;
	}

	/** 
	  * @param $photo
	  */
	public function setPhoto($photo)
	{
		$this->photo = $photo;
		return $this;
	}
	  
	/** 
	  * @param $pos
	  */
	public function setPos($pos)
	{
		$this->pos = $pos;
		return $this;
	}
  
	/** 
	  * @param $delay
	  */
	public function setDelay($delay)
	{
		$this->delay = $delay;
		return $this;
	}
}

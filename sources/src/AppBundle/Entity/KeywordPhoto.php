<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/** 
  * @ORM\Entity
  * @ORM\Table(name="pma_keywordphotos",
  *   uniqueConstraints={
  *     @ORM\UniqueConstraint(name="keywordphotos_idx", columns={"keywordId", "photoId"})
  *   }
  *  )
  */
class KeywordPhoto
{
	/** 
	  * @ORM\Column(type="integer",name="itemId",nullable=false)
	  * @ORM\Id
	  */
	private $itemId;

	/** 
      * @ORM\ManyToOne(targetEntity="Keyword")
      * @ORM\JoinColumn(name="keywordId", referencedColumnName="keywordId",nullable=false)
	  */
	private $keyword;

	/** 
      * @ORM\ManyToOne(targetEntity="Photo")
      * @ORM\JoinColumn(name="photoId", referencedColumnName="photoId",onDelete="CASCADE",nullable=false)
	  */
	private $photo;

	/** 
	  * @ORM\Column(type="smallint",name="score",nullable=false,options={"default":0})
	  */
	private $score;

	/** 
	  * @return $itemId
	  */
	public function getItemId()
	{
		return $this->itemId;
	}

	/** 
	  * @return $keyword
	  */
	public function getKeyword()
	{
		return $this->keyword;
	}

	/** 
	  * @return $photo
	  */
	public function getPhoto()
	{
		return $this->photo;
	}

	/** 
	  * @return $score
	  */
	public function getScore()
	{
		return $this->score;
	}

	/** 
	  * @param $itemId
	  */
	public function setItemId($itemId)
	{
		$this->itemId = $itemId;
		return $this;
	}

	/** 
	  * @param $keyword
	  */
	public function setKeyword($keyword)
	{
		$this->keyword = $keyword;
		return $this;
	}

	/** 
	  * @param $photo
	  */
	public function setPhoto($photo)
	{
		$this->photo = $photo;
		return $this;
	}
	  
	/** 
	  * @param $score
	  */
	public function setScore($score)
	{
		$this->score = $score;
		return $this;
	}
  
}

<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/** 
  * @ORM\Entity
  * @ORM\Table(name="pma_keywords")
  */
class Keyword
{
	/** 
	  * @ORM\Column(type="integer",name="keywordId",nullable=false)
	  * @ORM\Id
	  */
	private $keywordId;

	/** 
	  * @ORM\Column(type="string",name="keyword",length=25,unique=true,nullable=false,options={"collation":"utf16_unicode_ci"})
	  */
	private $keyword;

	/** 
	  * @return $keywordId
	  */
	public function getKeywordId()
	{
		return $this->keywordId;
	}

	/** 
	  * @return $keyword
	  */
	public function getKeyword()
	{
		return $this->keyword;
	}

	/** 
	  * @param $keywordId
	  */
	public function setKeywordId($keywordId)
	{
		$this->keywordId = $keywordId;
		return $this;
	}
	  
	/** 
	  * @param $keyword
	  */
	public function setKeyword($keyword)
	{
		$this->keyword = $keyword;
		return $this;
	}

	/** 
	  * @param to be displayed by Keyword lookup: 
      * display: keyword
      * refer: keywordId
	  */
    public function __toString() 
    {
        return $this->keyword;
    }    
   
}

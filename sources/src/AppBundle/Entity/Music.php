<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/** 
  * @ORM\Entity
  * @ORM\Table(name="pma_musics")
  */
class Music
{
	/** 
	  * @ORM\Column(type="integer",name="musicId",nullable=false)
	  * @ORM\Id
	  */
	private $musicId;

	/** 
	  * @ORM\Column(type="string",name="musicName",length=50,unique=true,nullable=false,options={"collation":"utf16_unicode_ci"})
	  */
	private $name;

	/** 
	  * @ORM\Column(type="string",name="musicLink",length=50,unique=true,nullable=false)
	  */
	private $link;

	/** 
	  * @return $musicId
	  */
	public function getMusicId()
	{
		return $this->musicId;
	}

	/** 
	  * @return $name
	  */
	public function getName()
	{
		return $this->name;
	}

	/** 
	  * @return $link
	  */
	public function getLink()
	{
		return $this->link;
	}

	/** 
	  * @param $musicId
	  */
	public function setMusicId($musicId)
	{
		$this->musicId = $musicId;
		return $this;
	}
	  
	/** 
	  * @param $name
	  */
	public function setName($name)
	{
		$this->name = $name;
		return $this;
	}

	/** 
	  * @param $link
	  */
	public function setLink($link)
	{
		$this->link = $link;
		return $this;
	}

	/** 
	  * @param to be displayed by Music lookup: 
      * display: name
      * refer: musicId
	  */
    public function __toString() 
    {
        return $this->name;
    }    
   
}

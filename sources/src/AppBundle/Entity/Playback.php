<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/** 
  * @ORM\Entity
  * @ORM\Table(name="pma_playbacks")
  */
class Playback
{
	/** 
	  * @ORM\Column(type="integer",name="playbackId",nullable=false)
	  * @ORM\Id
	  */
	private $playbackId;

	/** 
	  * @ORM\Column(type="string",name="playbackName",length=50,unique=true,nullable=false,options={"collation":"utf16_unicode_ci"})
	  */
	private $name;

	/** 
	  * @ORM\Column(type="smallint",name="status",nullable=false,options={"default":1})
	  */
	private $status;

  	/** 
	  * @ORM\Column(type="smallint",name="privacy",nullable=false,options={"default":0})
	  */
	private $privacy;

	/** 
	  * @ORM\Column(type="string",name="description",length=500,nullable=true,options={"collation":"utf16_unicode_ci"})
	  */
	private $description;


	/** 
	  * @return $playbackId
	  */
	public function getPlaybackId()
	{
		return $this->playbackId;
	}

	/** 
	  * @return $name
	  */
	public function getName()
	{
		return $this->name;
	}

	/** 
	  * @return $status
	  */
	public function getStatus()
	{
		return $this->status;
	}

	/** 
	  * @return $privacy
	  */
	public function getPrivacy()
	{
		return $this->privacy;
	}

	/** 
	  * @return $description
	  */
	public function getDescription()
	{
		return $this->description;
	}

	/** 
	  * @param $playbackId
	  */
	public function setPlaybackId($playbackId)
	{
		$this->playbackId = $playbackId;
		return $this;
	}
	  
	/** 
	  * @param $name
	  */
	public function setName($name)
	{
		$this->name = $name;
		return $this;
	}

	/** 
	  * @param $status
	  */
	public function setStatus($status)
	{
		$this->status = $status;
		return $this;
	}

	/** 
	  * @param $privacy
	  */
	public function setPrivacy($privacy)
	{
		$this->privacy = $privacy;
		return $this;
	}

	/** 
	  * @param $description
	  */
	public function setDescription($description)
	{
		$this->description = $description;
		return $this;
	}


	/** 
	  * @param to be displayed by Playback lookup: 
      * display: name
      * refer: playbackId
	  */
    public function __toString() 
    {
        return $this->name;
    }    
   
}

<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/** 
  * @ORM\Entity
  * @ORM\Table(name="pma_keywordvideos",
  *   uniqueConstraints={
  *     @ORM\UniqueConstraint(name="keywordphotos_idx", columns={"keywordId", "videoId"})
  *   }
  *  )
  */
class KeywordVideo
{
	/** 
	  * @ORM\Column(type="integer",name="itemId",nullable=false)
	  * @ORM\Id
	  */
	private $itemId;

	/** 
      * @ORM\ManyToOne(targetEntity="Keyword")
      * @ORM\JoinColumn(name="keywordId", referencedColumnName="keywordId",nullable=false)
	  */
	private $keyword;

	/** 
      * @ORM\ManyToOne(targetEntity="Video")
      * @ORM\JoinColumn(name="videoId", referencedColumnName="videoId",onDelete="CASCADE",nullable=false)
	  */
	private $video;

	/** 
	  * @ORM\Column(type="smallint",name="score",nullable=false,options={"default":0})
	  */
	private $score;

	/** 
	  * @return $itemId
	  */
	public function getItemId()
	{
		return $this->itemId;
	}

	/** 
	  * @return $keyword
	  */
	public function getKeyword()
	{
		return $this->keyword;
	}

	/** 
	  * @return $video
	  */
	public function getVideo()
	{
		return $this->video;
	}

	/** 
	  * @return $score
	  */
	public function getScore()
	{
		return $this->score;
	}

	/** 
	  * @param $itemId
	  */
	public function setItemId($itemId)
	{
		$this->itemId = $itemId;
		return $this;
	}

	/** 
	  * @param $keyword
	  */
	public function setKeyword($keyword)
	{
		$this->keyword = $keyword;
		return $this;
	}

	/** 
	  * @param $video
	  */
	public function setVideo($video)
	{
		$this->video = $video;
		return $this;
	}
	  
	/** 
	  * @param $score
	  */
	public function setScore($score)
	{
		$this->score = $score;
		return $this;
	}
  
}

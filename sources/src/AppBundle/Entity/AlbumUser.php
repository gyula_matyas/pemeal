<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/** 
  * @ORM\Entity
  * @ORM\Table(name="pma_albumusers",
  *   uniqueConstraints={
  *     @ORM\UniqueConstraint(name="albumusers_idx", columns={"albumId", "userId"})
  *   }
  *  )
  */
class AlbumUser
{
	/** 
	  * @ORM\Column(type="integer",name="itemId",nullable=false)
	  * @ORM\Id
	  */
	private $itemId;

	/** 
      * @ORM\ManyToOne(targetEntity="Album")
      * @ORM\JoinColumn(name="albumId", referencedColumnName="albumId",onDelete="CASCADE",nullable=false)
	  */
	private $album;

	/** 
      * @ORM\ManyToOne(targetEntity="User")
      * @ORM\JoinColumn(name="userId", referencedColumnName="userId",onDelete="CASCADE",nullable=false)
	  */
	private $user;

	/** 
	  * @return $itemId
	  */
	public function getItemId()
	{
		return $this->itemId;
	}

	/** 
	  * @return $album
	  */
	public function getAlbum()
	{
		return $this->album;
	}

	/** 
	  * @return $user
	  */
	public function getUser()
	{
		return $this->user;
	}

	/** 
	  * @param $itemId
	  */
	public function setItemId($itemId)
	{
		$this->itemId = $itemId;
		return $this;
	}

	/** 
	  * @param $album
	  */
	public function setAlbum($album)
	{
		$this->album = $album;
		return $this;
	}

	/** 
	  * @param $user
	  */
	public function setUser($user)
	{
		$this->user = $user;
		return $this;
	}
	  
  
}

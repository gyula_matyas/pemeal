<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/** 
  * @ORM\Entity
  * @ORM\Table(name="pma_playbackmusics",
  *   uniqueConstraints={
  *     @ORM\UniqueConstraint(name="playbackmusics_idx", columns={"playbackId", "musicId"})
  *   }
  *  )
  */
class PlaybackMusic
{
	/** 
	  * @ORM\Column(type="integer",name="itemId",nullable=false)
	  * @ORM\Id
	  */
	private $itemId;

	/** 
      * @ORM\ManyToOne(targetEntity="Playback")
      * @ORM\JoinColumn(name="playbackId", referencedColumnName="playbackId",onDelete="CASCADE",nullable=false)
	  */
	private $playback;

	/** 
      * @ORM\ManyToOne(targetEntity="Music")
      * @ORM\JoinColumn(name="musicId", referencedColumnName="musicId",onDelete="CASCADE",nullable=false)
	  */
	private $music;

	/** 
	  * @ORM\Column(type="smallint",name="pos",nullable=false,options={"default":0})
	  */
	private $pos;

	/** 
	  * @ORM\Column(type="integer",name="repeat",nullable=false,options={"default":1})
	  */
	private $repeat;

	/** 
	  * @return $itemId
	  */
	public function getItemId()
	{
		return $this->itemId;
	}

	/** 
	  * @return $playback
	  */
	public function getPlayback()
	{
		return $this->playback;
	}

	/** 
	  * @return $music
	  */
	public function getMusic()
	{
		return $this->music;
	}

	/** 
	  * @return $pos
	  */
	public function getPos()
	{
		return $this->pos;
	}

	/** 
	  * @return $repeat
	  */
	public function getRepeat()
	{
		return $this->repeat;
	}

	/** 
	  * @param $itemId
	  */
	public function setItemId($itemId)
	{
		$this->itemId = $itemId;
		return $this;
	}

	/** 
	  * @param $playback
	  */
	public function setPlayback($playback)
	{
		$this->playback = $playback;
		return $this;
	}

	/** 
	  * @param $music
	  */
	public function setMusic($music)
	{
		$this->music = $music;
		return $this;
	}
	  
	/** 
	  * @param $pos
	  */
	public function setPos($pos)
	{
		$this->pos = $pos;
		return $this;
	}
  
	/** 
	  * @param $repeat
	  */
	public function setRepeat($repeat)
	{
		$this->repeat = $repeat;
		return $this;
	}
}

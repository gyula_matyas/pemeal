<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/** 
  * @ORM\Entity
  * @ORM\Table(name="pma_photos")
  */
class Photo
{
	/** 
	  * @ORM\Column(type="integer",name="photoId",nullable=false)
	  * @ORM\Id
	  */
	private $photoId;

	/** 
	  * @ORM\Column(type="string",name="photoName",length=100,nullable=false,options={"collation":"utf16_unicode_ci"})
	  */
	private $name;

	/** 
	  * @ORM\Column(type="string",name="location",length=100,nullable=false,options={"collation":"utf16_unicode_ci"})
	  */
	private $location;

	/** 
	  * @ORM\Column(type="string",name="photoLink",length=50,nullable=false,unique=true)
	  */
	private $link;

	/** 
	  * @ORM\Column(type="smallint",name="status",nullable=false,options={"default":1})
	  */
	private $status;

  	/** 
	  * @ORM\Column(type="smallint",name="privacy",nullable=false,options={"default":0})
	  */
	private $privacy;

	/** 
      * @ORM\ManyToOne(targetEntity="Album")
      * @ORM\JoinColumn(name="albumId", referencedColumnName="albumId",nullable=false)
	  */
	private $album;

  	/** 
	  * @ORM\Column(type="date",name="photoDate",nullable=false)
	  */
	private $date;

	/** 
	  * @ORM\Column(type="string",name="description",length=500,nullable=true,options={"collation":"utf16_unicode_ci"})
	  */
	private $description;

	/** 
	  * @return $photoId
	  */
	public function getPhotoId()
	{
		return $this->photoId;
	}

	/** 
	  * @return $name
	  */
	public function getName()
	{
		return $this->name;
	}

	/** 
	  * @return $location
	  */
	public function getLocation()
	{
		return $this->location;
	}

	/** 
	  * @return $link
	  */
	public function getlink()
	{
		return $this->link;
	}

	/** 
	  * @return $status
	  */
	public function getStatus()
	{
		return $this->status;
	}

	/** 
	  * @return $privacy
	  */
	public function getPrivacy()
	{
		return $this->privacy;
	}

	/** 
	  * @return $album
	  */
	public function getAlbum()
	{
		return $this->album;
	}

	/** 
	  * @return $date
	  */
	public function getDate()
	{
		return $this->date;
	}

	/** 
	  * @return $description
	  */
	public function getDescription()
	{
		return $this->description;
	}

	/** 
	  * @param $photoId
	  */
	public function setPhotoId($photoId)
	{
		$this->photoId = $photoId;
		return $this;
	}
	  
	/** 
	  * @param $name
	  */
	public function setName($name)
	{
		$this->name = $name;
		return $this;
	}

	/** 
	  * @param $location
	  */
	public function setLocation($location)
	{
		$this->location = $location;
		return $this;
	}

	/** 
	  * @param $link
	  */
	public function setLink($link)
	{
		$this->link = $link;
		return $this;
	}

	/** 
	  * @param $status
	  */
	public function setStatus($status)
	{
		$this->status = $status;
		return $this;
	}

	/** 
	  * @param $privacy
	  */
	public function setPrivacy($privacy)
	{
		$this->privacy = $privacy;
		return $this;
	}

	/** 
	  * @param $album
	  */
	public function setAlbum($album)
	{
		$this->album = $album;
		return $this;
	}

	/** 
	  * @param $date
	  */
	public function setDate($date)
	{
		$this->date = $date;
		return $this;
	}

	/** 
	  * @param $description
	  */
	public function setDescription($description)
	{
		$this->description = $description;
		return $this;
	}

	/** 
	  * @param to be displayed by photo lookup: 
      * display: photoId (photoName) - photoLink
      * refer: photoId
	  */
    public function __toString() 
    {
		if (isnull($this->photoLink))
		{
			return str($this->photoId) . " - " . $this->photoLink;
		}
		else
		{
			return str($this->photoId) . " (" . $this->photoName . ") - " . $this->photoLink;
		}
    }
   
}

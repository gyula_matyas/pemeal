<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/** 
  * @ORM\Entity
  * @ORM\Table(name="pma_users")
  */
class User
{
	/** 
	  * @ORM\Column(type="integer",name="userId",nullable=false)
	  * @ORM\Id
	  */
	private $userId;

	/** 
	  * @ORM\Column(type="string",name="userName",length=50,unique=true,nullable=false)
	  */
	private $name;

	/** 
	  * @ORM\Column(type="string",name="userPwd",length=50,nullable=false)
	  */
	private $pwd;

	/** 
	  * @ORM\Column(type="string",name="nickName",length=50,nullable=true,options={"collation":"utf16_unicode_ci"})
	  */
	private $nickName;

	/** 
	  * @ORM\Column(type="smallint",name="userLevel",nullable=false,options={"default":0})
	  */
	private $level;

	/** 
	  * @ORM\Column(type="smallint",name="status",nullable=false,options={"default":0})
	  */
	private $status;

	/** 
	  * @ORM\Column(type="string",name="lngCode",nullable=true,length=3)
	  */
	private $language;

  	/** 
	  * @ORM\Column(type="string",name="email",nullable=false,length=100,unique=true)
	  */
	private $email;

  	/** 
	  * @ORM\Column(type="string",name="phone",nullable=false,length=12)
	  */
	private $phone;

	/** 
	  * @return $userId
	  */
	public function getUserId()
	{
		return $this->userId;
	}

	/** 
	  * @return $name
	  */
	public function getName()
	{
		return $this->name;
	}

	/** 
	  * @return $pwd
	  */
	public function getPwd()
	{
		return $this->pwd;
	}

	/** 
	  * @return $nickName
	  */
	public function getNickName()
	{
		return $this->nickName;
	}

	/** 
	  * @return $level
	  */
	public function getLevel()
	{
		return $this->level;
	}

	/** 
	  * @return $status
	  */
	public function getStatus()
	{
		return $this->status;
	}

	/** 
	  * @return $language
	  */
	public function getLanguage()
	{
		return $this->language;
	}

	/** 
	  * @return $email
	  */
	public function getEmail()
	{
		return $this->email;
	}

	/** 
	  * @return $phone
	  */
	public function getPhone()
	{
		return $this->phone;
	}

	/** 
	  * @param $userid
	  */
	public function setUserId($userId)
	{
		$this->userId = $userId;
	}
	  
	/** 
	  * @param $name
	  */
	public function setName($name)
	{
		$this->name = $name;
	}

	/** 
	  * @param $pwd
	  */
	public function setPwd($pwd)
	{
		$this->pwd = $pwd;
	}

	/** 
	  * @param $nickName
	  */
	public function setNickName($nickName)
	{
		$this->nickName = $nickName;
	}

	/** 
	  * @param $level
	  */
	public function setLevel($level)
	{
		$this->level = $level;
	}

	/** 
	  * @param $status
	  */
	public function setStatus($status)
	{
		$this->status = $status;
	}

	/** 
	  * @param $language
	  */
	public function setLanguage($language)
	{
		$this->language = $language;
	}

	/** 
	  * @param $email
	  */
	public function setEmail($email)
	{
		$this->email = $email;
	}

	/** 
	  * @param $phone
	  */
	public function setPhone($phone)
	{
		$this->phone = $phone;
	}

	/** 
	  * @param to be displayed by User lookup: 
      * display: nick name (name)
      * refer: userId
	  */
    public function __toString() 
    {
		if (isnull($this->name))
		{
			return $this->name;
		}
		else	
		{
			return $this->nickName . " (" . $this->name . ")";
		}
    }    
   
}

<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/** 
  * @ORM\Entity
  * @ORM\Table(name="pma_photousers",
  *   uniqueConstraints={
  *     @ORM\UniqueConstraint(name="photousers_idx", columns={"photoId", "userId"})
  *   }
  *  )
  */
class PhotoUser
{
	/** 
	  * @ORM\Column(type="integer",name="itemId",nullable=false)
	  * @ORM\Id
	  */
	private $itemId;

	/** 
      * @ORM\ManyToOne(targetEntity="Photo")
      * @ORM\JoinColumn(name="photoId", referencedColumnName="photoId",onDelete="CASCADE",nullable=false)
	  */
	private $photo;

	/** 
      * @ORM\ManyToOne(targetEntity="User")
      * @ORM\JoinColumn(name="userId", referencedColumnName="userId",onDelete="CASCADE",nullable=false)
	  */
	private $user;

	/** 
	  * @return $itemId
	  */
	public function getItemId()
	{
		return $this->itemId;
	}

	/** 
	  * @return $photo
	  */
	public function getPhoto()
	{
		return $this->photo;
	}

	/** 
	  * @return $user
	  */
	public function getUser()
	{
		return $this->user;
	}

	/** 
	  * @param $itemId
	  */
	public function setItemId($itemId)
	{
		$this->itemId = $itemId;
		return $this;
	}

	/** 
	  * @param $photo
	  */
	public function setPhoto($photo)
	{
		$this->photo = $photo;
		return $this;
	}

	/** 
	  * @param $user
	  */
	public function setUser($user)
	{
		$this->user = $user;
		return $this;
	}
	  
  
}

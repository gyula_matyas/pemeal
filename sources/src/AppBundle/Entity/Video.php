<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/** 
  * @ORM\Entity
  * @ORM\Table(name="pma_videos")
  */
class Video
{
	/** 
	  * @ORM\Column(type="integer",name="videoId",nullable=false)
	  * @ORM\Id
	  */
	private $videoId;

	/** 
	  * @ORM\Column(type="string",name="videoName",length=100,nullable=false,options={"collation":"utf16_unicode_ci"})
	  */
	private $name;

	/** 
	  * @ORM\Column(type="string",name="location",length=100,nullable=false,options={"collation":"utf16_unicode_ci"})
	  */
	private $location;

	/** 
	  * @ORM\Column(type="string",name="videoLink",length=50,nullable=false,unique=true)
	  */
	private $link;

	/** 
	  * @ORM\Column(type="smallint",name="status",nullable=false,options={"default":1})
	  */
	private $status;

  	/** 
	  * @ORM\Column(type="smallint",name="privacy",nullable=false,options={"default":0})
	  */
	private $privacy;

	/** 
      * @ORM\ManyToOne(targetEntity="Album")
      * @ORM\JoinColumn(name="albumId", referencedColumnName="albumId",nullable=false)
	  */
	private $album;

  	/** 
	  * @ORM\Column(type="date",name="videoDate",nullable=false)
	  */
	private $date;

	/** 
	  * @ORM\Column(type="string",name="description",length=500,nullable=true,options={"collation":"utf16_unicode_ci"})
	  */
	private $description;

	/** 
	  * @return $videoId
	  */
	public function getVideoId()
	{
		return $this->videoId;
	}

	/** 
	  * @return $name
	  */
	public function getName()
	{
		return $this->name;
	}

	/** 
	  * @return $location
	  */
	public function getLocation()
	{
		return $this->location;
	}

	/** 
	  * @return $link
	  */
	public function getlink()
	{
		return $this->link;
	}

	/** 
	  * @return $status
	  */
	public function getStatus()
	{
		return $this->status;
	}

	/** 
	  * @return $privacy
	  */
	public function getPrivacy()
	{
		return $this->privacy;
	}

	/** 
	  * @return $album
	  */
	public function getAlbum()
	{
		return $this->album;
	}

	/** 
	  * @return $date
	  */
	public function getDate()
	{
		return $this->date;
	}

	/** 
	  * @return $description
	  */
	public function getDescription()
	{
		return $this->description;
	}

	/** 
	  * @param $videoId
	  */
	public function setVideoId($videoId)
	{
		$this->videoId = $videoId;
		return $this;
	}
	  
	/** 
	  * @param $name
	  */
	public function setName($name)
	{
		$this->name = $name;
		return $this;
	}

	/** 
	  * @param $location
	  */
	public function setLocation($location)
	{
		$this->location = $location;
		return $this;
	}

	/** 
	  * @param $link
	  */
	public function setLink($link)
	{
		$this->link = $link;
		return $this;
	}

	/** 
	  * @param $status
	  */
	public function setStatus($status)
	{
		$this->status = $status;
		return $this;
	}

	/** 
	  * @param $privacy
	  */
	public function setPrivacy($privacy)
	{
		$this->privacy = $privacy;
		return $this;
	}

	/** 
	  * @param $album
	  */
	public function setAlbum($album)
	{
		$this->album = $album;
		return $this;
	}

	/** 
	  * @param $date
	  */
	public function setDate($date)
	{
		$this->date = $date;
		return $this;
	}

	/** 
	  * @param $description
	  */
	public function setDescription($description)
	{
		$this->description = $description;
		return $this;
	}

	/** 
	  * @param to be displayed by video lookup: 
      * display: videoId (videoName) - videoLink
      * refer: videoId
	  */
    public function __toString() 
    {
		if (isnull($this->videoLink))
		{
			return str($this->videoId) . " - " . $this->videoLink;
		}
		else
		{
			return str($this->videoId) . " (" . $this->videoName . ") - " . $this->videoLink;
		}
    }    
   
}

<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/** 
  * @ORM\Entity
  * @ORM\Table(name="pma_keys")
  */
class Key
{
	/** 
	  * @ORM\Column(type="integer",name="keyId",nullable=false)
	  * @ORM\Id
	  */
	private $id;

	/** 
	  * @ORM\Column(type="integer",name="keyValue",nullable=false,options={"default":1})
	  */
	private $value;

	/** 
	  * @return $id
	  */
	public function getId()
	{
		return $this->id;
	}

	/** 
	  * @return $value
	  */
	public function getValue()
	{
		return $this->value;
	}

	/** 
	  * @param $id
	  */
	public function setId($id)
	{
		$this->id = $id;
		return $this;
	}
	  
	/** 
	  * @param $value
	  */
	public function setValue($value)
	{
		$this->value = $value;
		return $this;
	}
  
}

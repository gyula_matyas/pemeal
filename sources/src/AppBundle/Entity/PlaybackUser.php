<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/** 
  * @ORM\Entity
  * @ORM\Table(name="pma_playbackusers",
  *   uniqueConstraints={
  *     @ORM\UniqueConstraint(name="playbackusers_idx", columns={"playbackId", "userId"})
  *   }
  *  )
  */
class PlaybackUser
{
	/** 
	  * @ORM\Column(type="integer",name="itemId",nullable=false)
	  * @ORM\Id
	  */
	private $itemId;

	/** 
      * @ORM\ManyToOne(targetEntity="Playback")
      * @ORM\JoinColumn(name="playbackId", referencedColumnName="playbackId",onDelete="CASCADE",nullable=false)
	  */
	private $playback;

	/** 
      * @ORM\ManyToOne(targetEntity="User")
      * @ORM\JoinColumn(name="userId", referencedColumnName="userId",onDelete="CASCADE",nullable=false)
	  */
	private $user;

	/** 
	  * @return $itemId
	  */
	public function getItemId()
	{
		return $this->itemId;
	}

	/** 
	  * @return $playback
	  */
	public function getPlayback()
	{
		return $this->playback;
	}

	/** 
	  * @return $user
	  */
	public function getUser()
	{
		return $this->user;
	}

	/** 
	  * @param $itemId
	  */
	public function setItemId($itemId)
	{
		$this->itemId = $itemId;
		return $this;
	}

	/** 
	  * @param $playback
	  */
	public function setPlayback($playback)
	{
		$this->playback = $playback;
		return $this;
	}

	/** 
	  * @param $user
	  */
	public function setUser($user)
	{
		$this->user = $user;
		return $this;
	}
	  
  
}

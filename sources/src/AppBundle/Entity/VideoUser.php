<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/** 
  * @ORM\Entity
  * @ORM\Table(name="pma_videousers",
  *   uniqueConstraints={
  *     @ORM\UniqueConstraint(name="videousers_idx", columns={"videoId", "userId"})
  *   }
  *  )
  */
class VideoUser
{
	/** 
	  * @ORM\Column(type="integer",name="itemId",nullable=false)
	  * @ORM\Id
	  */
	private $itemId;

	/** 
      * @ORM\ManyToOne(targetEntity="Video")
      * @ORM\JoinColumn(name="videoId", referencedColumnName="videoId",onDelete="CASCADE",nullable=false)
	  */
	private $video;

	/** 
      * @ORM\ManyToOne(targetEntity="User")
      * @ORM\JoinColumn(name="userId", referencedColumnName="userId",onDelete="CASCADE",nullable=false)
	  */
	private $user;

	/** 
	  * @return $itemId
	  */
	public function getItemId()
	{
		return $this->itemId;
	}

	/** 
	  * @return $video
	  */
	public function getVideo()
	{
		return $this->video;
	}

	/** 
	  * @return $user
	  */
	public function getUser()
	{
		return $this->user;
	}

	/** 
	  * @param $itemId
	  */
	public function setItemId($itemId)
	{
		$this->itemId = $itemId;
		return $this;
	}

	/** 
	  * @param $video
	  */
	public function setVideo($video)
	{
		$this->video = $video;
		return $this;
	}

	/** 
	  * @param $user
	  */
	public function setUser($user)
	{
		$this->user = $user;
		return $this;
	}
	  
  
}
